import { runSelectQuery } from './SparqlApi'
import {
  mapNameSampoResults
} from './Mappers'

const getResults = async (federatedSearchDatasets, queryTerm, latMin, longMin, latMax, longMax, datasetId, resultFormat) => {
  const { endpoint, useAuth, resultQuery, geoQuery, textQuery } = federatedSearchDatasets[datasetId]
  let query = ''
  if (datasetId !== 'tgn') {
    if (queryTerm !== '') {
      query = resultQuery.replace('<QUERY>', textQuery.replace('<QUERYSTRING>', queryTerm.toLowerCase()))
    } else if (latMin !== 0) {
      query = resultQuery.replace(/<QUERY>/g, geoQuery).replace(/<LATMIN>/g, latMin).replace(/<LONGMIN>/g, longMin).replace(/<LATMAX>/g, latMax).replace(/<LONGMAX>/g, longMax)
    }
  } else {
    query = resultQuery.replace(/<QUERYTERM>/g, queryTerm.toLowerCase())
  }
  console.log("QUERY", query)
  return runSelectQuery({
    query,
    endpoint,
    useAuth,
    resultMapper: mapNameSampoResults,
    resultFormat
  })
}

export const getFederatedResults = async ({
  federatedSearchDatasets,
  queryTerm,
  latMin,
  longMin,
  latMax,
  longMax,
  datasets,
  resultFormat
}) => {
  const federatedResults = await Promise.all(datasets.map((datasetId) =>
    getResults(federatedSearchDatasets, queryTerm, latMin, longMin, latMax, longMax, datasetId, resultFormat)))

  // merge search results from multiple endpoints into a single array
  let results = []
  federatedResults.forEach(resultSet => {
    results = [...results, ...resultSet.data]
  })

  return results
}

export const federatedSearchSparqlQueries = {
  norse: {
    geoQuery: `
    { ?place geof:within ( <LATMIN> <LONGMIN> <LATMAX> <LONGMAX>) }
    `,
    textQuery:  "?name rdfs:label/<tag:stardog:api:property:textMatch> '<QUERYSTRING>' .",
    resultQuery: `
    PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
    PREFIX wgs: <http://www.w3.org/2003/01/geo/wgs84_pos#>
    PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
    SELECT DISTINCT ?id ?prefLabel ?documentLabel ?document ?referenceLabel ?reference ?broaderTypeLabel ?broaderTypeLabel ?broaderAreaLabel ?coordinates ?lat ?long ?source WHERE { GRAPH <https://norseworld.nordiska.uu.se> {
    <QUERY>
    ?id crm:P1_is_identified_by ?name .
    ?name rdfs:label ?prefLabel .
  
    
    ?id wgs:lat ?lat;
      wgs:long ?long ;
      <https://norseworld.nordiska.uu.se/Type_of_locality> ?broaderTypeLabel .

      BIND(?id as ?coordinates)
      BIND("Norseworld" AS ?source)

      OPTIONAL {
        ?id crm:P70i_is_documented_in ?document .
        ?document rdfs:label ?documentLabel .
      }

      OPTIONAL {
        ?id crm:P67i_is_referred_to_by ?reference .
        ?reference rdfs:label ?referenceLabel .
      }
  }
}
    `
  },
  saga: {
    geoQuery: `{ ?id geof:within ( <LATMIN> <LONGMIN> <LATMAX> <LONGMAX>) }`,
    textQuery:  "?id rdfs:label/<tag:stardog:api:property:textMatch> '<QUERYSTRING>' .",
    resultQuery: `
        PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
        PREFIX wgs: <http://www.w3.org/2003/01/geo/wgs84_pos#>
        PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
        PREFIX la: <https://linked.art/ns/terms/>
        SELECT DISTINCT ?id ?prefLabel ?document ?documentLabel ?documentURL ?reference ?referenceLabel ?broaderType ?broaderTypeLabel ?broaderAreaLabel ?lat ?long ?coordinates ?source WHERE {GRAPH <https://sagamap.is/graphs/> {
        <QUERY>
        ?id rdfs:label ?prefLabel .
        ?id crm:P2_has_type ?broaderType .
        ?broaderType rdfs:label ?broaderTypeLabel .
        FILTER (?broaderTypeLabel != "Place name")
        
        ?id crm:P89_falls_within/rdfs:label ?broaderAreaLabel ;
            wgs:lat ?lat;
            wgs:long ?long .

        BIND("Sagamap" AS ?source)

        OPTIONAL {
          ?document crm:P70_documents ?id .
          ?document rdfs:label ?documentLabel .
          OPTIONAL { ?document la:access_point ?documewntURL}
        }
  
        OPTIONAL {
          ?id crm:P70_documents ?reference .
          ?reference rdfs:label ?referenceLabel .
        }
    }}
    `
  },
  nsdb: {
    geoQuery: `{ ?coordinates geof:within ( <LATMIN> <LONGMIN> <LATMAX> <LONGMAX>) .
                 ?place crm:P189i_is_approximated_by ?coordinates .}`,
    textQuery:  "?id rdfs:label/<tag:stardog:api:property:textMatch> '<QUERYSTRING>' .",
    resultQuery: `
    PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    PREFIX geof: <http://www.opengis.net/def/function/geosparql/>

      SELECT DISTINCT ?id ?prefLabel ?broaderType ?broaderTypeLabel ?broaderAreaLabel ?coordinates ?lat ?long ?source WHERE  { GRAPH <https://data.spraksamlingane.no/stadnamn/nos-hs/nsdb> {
        <QUERY>
        ?id crm:P1i_identifies ?place ;
            rdfs:label ?prefLabel .


        ?place crm:P2_has_type ?broaderType .
        GRAPH <https://data.spraksamlingane.no/stadnamn/nos-hs/sosi> {
          ?broaderType skos:prefLabel ?broaderTypeLabel .
        }

        ?place crm:P189i_is_approximated_by ?municipality .

        ?municipality rdfs:label ?municipalityLabel .
        ?municipality crm:P89_falls_within ?county .
        ?county rdfs:label ?countyLabel .
        
        BIND(CONCAT(?municipalityLabel, ", ", ?countyLabel) as ?broaderAreaLabel)

        ?place crm:P189i_is_approximated_by ?coordinates .
        ?coordinates crm:P168_place_is_defined_by ?point;
                    crm:P2_has_type ?coordinate_type .
                bind( replace( str(?point), "^[^0-9\\\\.]*([0-9\\\\.]+) .*$", "$1" ) as ?long )
                bind( replace( str(?point), "^.* ([0-9\\\\.]+)[^0-9\\\\.]*$", "$1" ) as ?lat )


      BIND("NSDB" AS ?source)
      BIND("violet" AS ?markerColor)

      }
    }
    `
  },
  tgn: {
    // Getty LOD documentation:
    // http://vocab.getty.edu/queries#Places_by_Type
    // https://groups.google.com/forum/#!topic/gettyvocablod/r4wsSJyne84
    // https://confluence.ontotext.com/display/OWLIMv54/OWLIM-SE+Full-text+Search
    // http://vocab.getty.edu/queries#Combination_Full-Text_and_Exact_String_Match
    // http://vocab.getty.edu/doc/#TGN_Place_Types
    resultQuery: `
        SELECT ?id (COALESCE(?labelEn,?labelGVP) AS ?prefLabel) ?broaderTypeLabel
          ?broaderAreaLabel ?source ?lat ?long ?markerColor
        WHERE {
            ?id luc:term "<QUERYTERM>" ;
            skos:inScheme tgn: ;
            gvp:placeTypePreferred [
              gvp:prefLabelGVP [
                xl:literalForm ?broaderTypeLabel;
                dct:language gvp_lang:en
              ]
            ];
            gvp:broaderPreferred/xl:prefLabel/xl:literalForm ?broaderAreaLabel .
          OPTIONAL {
            ?id xl:prefLabel [
              xl:literalForm ?labelEn ;
              dct:language gvp_lang:en
            ]
          }
          OPTIONAL {
            ?id gvp:prefLabelGVP [xl:literalForm ?labelGVP]
          }
          OPTIONAL {
            ?id foaf:focus ?place .
            ?place wgs:lat ?lat ;
                   wgs:long ?long .
          }
          FILTER EXISTS {
            ?id xl:prefLabel/gvp:term+?term .
            FILTER (LCASE(STR(?term))="<QUERYTERM>")
          }
          BIND("TGN" AS ?source)
          BIND("orange" AS ?markerColor)
        }
        `
  },
  kotus: {
    resultQuery: `
        PREFIX text: <http://jena.apache.org/text#>
        PREFIX spatial: <http://jena.apache.org/spatial#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
        PREFIX gs: <http://www.opengis.net/ont/geosparql#>
        PREFIX hipla-schema: <http://ldf.fi/schema/hipla/>
        PREFIX na-schema: <http://ldf.fi/schema/kotus-names-archive/>
        PREFIX wgs84: <http://www.w3.org/2003/01/geo/wgs84_pos#>
        SELECT ?id ?prefLabel ?namesArchiveLink ?typeLabel ?broaderTypeLabel
        ?broaderAreaLabel ?source ?lat ?long ?modifier ?basicElement ?collector
        ?collectionYear ?markerColor ?positioningAccuracy
        WHERE {
          <QUERY>
          ?id skos:prefLabel ?prefLabel .
          ?id na-schema:parish ?broaderAreaLabel .
          ?id owl:sameAs ?namesArchiveLink .
          BIND("NA" AS ?source)
          BIND("violet" AS ?markerColor)
          BIND("-" AS ?missingValue)
          OPTIONAL {
            ?id a ?type .
            OPTIONAL {
              ?type skos:prefLabel ?typeLabel_ .
              ?type rdfs:subClassOf/skos:prefLabel ?broaderTypeLabel_ .
            }
          }
          BIND(COALESCE(?typeLabel_, ?missingValue) as ?typeLabel)
          BIND(COALESCE(?broaderTypeLabel_, ?missingValue) as ?broaderTypeLabel)
          OPTIONAL {
            ?id wgs84:lat ?lat .
            ?id wgs84:long ?long .
          }
          OPTIONAL { ?id na-schema:positioning_accuracy ?positioningAccuracy }
          OPTIONAL {
            ?id na-schema:place_name_modifier ?modifier ;
               na-schema:place_name_basic_element ?basicElement .
          }
          OPTIONAL { ?id na-schema:collector ?collector }
          OPTIONAL { ?id na-schema:stamp_date ?collectionYear }
        }
      `
  }
}
